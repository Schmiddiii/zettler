---
title: Renaming Notes
tags: [feature]
---

You can easily rename notes just by changing its frontmatter.

By default, the following naming convention is defines (where the first matching is applied):

- if it contains the tag `daily` and contains `date`, name it `daily/{date}.md`.
- if it contains the tag `weekly` and contains `title`, name it `weekly/{title}.md`.
- if it contains contains `title` and `id`, name it `{id}-{title}.md`.
- if it contains contains `title`, name it `{title}.md`.
- if it contains contains `id`, name it `{id}.md`.
- name it `unnamed.md`.

Note that both the file itself will be renamed and all references to it (both in opened files, which will be saved afterwards, and all other notes).
Furthermore, note that it currently assumes any folders you reference already exist.
When the file you want to rename to already exists, nothing will be done.
If you are editing a template file (i.e. files in the `templates` folder), the file will not be renamed.

Feel free to try out the renaming with this file.
Note that the reference to this file in [[Zettler]] will be replaced, whether or not it is opened in the editor.

This naming convention is intended to be modified by the user in the file `src/file_naming_rules.rs`, via the global variable `NAMING_RULES` and custom implementations of `FileNamingRule`.
