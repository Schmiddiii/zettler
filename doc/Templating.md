---
title: Templating
tags: []
---

Templates allow for information to be inserted during note creation.
This allows e.g. to give the note an unique ID based on the current date and time.

Templates are just normal markdown documents, which will be copied over during note creation.
Words inside `{{` and `}}` denotes template variables, which will be replaced when creating the note.
The following table summarizes the list of default tamplating variables with example replacements and descriptions:

| Variable   | Example                 | Description                                                              |
|------------|-------------------------|--------------------------------------------------------------------------|
| {{uuid}}   | 202404051226            | The current date and time in format `%Y%m%d%H%M`.                        |
| {{date}}   | 2024-04-05              | The current date in format `%F`.                                         |
| {{hdate}}  | Friday, April 5th, 2024 | Human-readable date including weekday, month, day and year.              |
| {{year}}   | 2024                    | The current year in format `%Y`.                                         |
| {{week}}   | 14                      | The current week in format `%V`.                                         |
| {{cursor}} |                         | A special key which will be removed and where the cursor will be placed. |

This list is intended to be manually extended by the user based on their needs, for that just add to `Templating::templates` in `src/templating.rs`.
