---
title: Completion
tags: [feature]
---

Links can be auto-completed.
Just type `[[` and a popup should appear listing all your notes.
You can continue typing to filter this list, accepting the completion fills the link and leaves the cursor to after the link.

Try it out by inserting a link to the Zettler note in the next line!

Note that for the completion to pop up, you will need to have closing `]` auto-inserting, as the internal implementation looks for text inside `[[` and `]]` and completes based on this.
