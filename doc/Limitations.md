---
title: Limitations
tags: []
---


- Helix: The language server will not be able to rename files or replace references when using `:wq` (i.e. quitting directly after writing).
  This is due to Helix not sending `textDocument/didSave` notifications.
- Helix: Before version `24.03`, renaming the current note did not rename the buffer, which can lead to faulty behavior (e.g. both the old and new filename existing at once).
- Only supports UTF-8 clients.
  This suffices for my use case.
- Even UTF-8 may have some issues I did not yet test.
- It currently only supports local files, remote files are not supported.
- When renaming files, all notes in the Zettelkasten will be read.
  Zettler does not yet have an internal database of links.
- When editing a document, the entire contents of the document will be sent to Zettler instead of only the change.
  Zettler does not yet implement partial syncing.
- Zettler regularly queries the entire list of notes (recursively) instead of partially updating it.
  It does not yet implement partially updating it using `inotify`.
- Currently only supports links to documents themselves, not to e.g. heading of those documents.
- There is currently no easy way to let the user choose which template to use when creating a file.
  I am unsure how to map this to the LSP model.
  Feel free to suggest something.
- Searching for notes is currently non-fuzzy.
