---
title: Link Styles
tags: []
---

Supported are currently only "trivial" media-wiki, i.e.:

- A link has to start with `[[` and end with `]]`, which must be within the same line.
- The link content is the file path relative to the zettelkasten directory and without the file ending.

Actions on links, e.g. [[Go to Definition]] are valid from the first `[` until the last `]`.
