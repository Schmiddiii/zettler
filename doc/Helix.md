---
title: Helix
tags: [editor]
website: "https://helix-editor.com/"
---

You can configure helix to use [[Zettler]] in your Zettelkasten by adding the following in `languages.toml`:

```toml
[[language]]
name = "markdown"
soft-wrap.enable = true
language-servers = [{ name = "zettler" }]

[language-server.zettler]
command = "zettler"
config = { zettelkasten-path = "/<path>/<to>/<your>/<zettelkasten>" }
```

You may also want to add key-bindings to create new notes in your `config.toml`, e.g. `<space>-z-n` to create a new note, or `<space>-z-d` to create a daily note:

```toml
[keys.normal."space".z]
n = ":lsp-workspace-command new-note"
d = ":lsp-workspace-command daily"
w = ":lsp-workspace-command weekly"
```
