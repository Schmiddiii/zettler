---
title: Configuration
tags: []
---

[[Zettler]] was intended for the author personally.
It therefore is barely configurable.
Configuration of Zettler should mostly be done by modifying the code itself, e.g. to modify [[Renaming Notes]] or [[Templating]].

Zettler provides only one mandatory configuration option, which specifies the location of the zettelkasten.
An example (json) configuration of Zettler could therefore look like this:

```json
{
  "zettelkasten-path": "/<path>/<to>/<your>/<zettelkasten>"
}
```

For an example on how to apply this to your editor, see e.g. [[Helix]].
