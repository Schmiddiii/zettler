---
title: Hover
tags: [feature]
---

You can hover over links to view their first (non-frontmatter) line.

Feel free to hover the following link, you will see the first paragraph of the Zettler note:

[[Zettler]]

Please also inspect the [[Link Styles]] currently allowed.
