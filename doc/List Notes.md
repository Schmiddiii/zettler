---
title: List Notes
tags: [feature]
---

You can list all notes and jump to them using the "list workspace symbols" feature of the LSP.

Try it out and jump back to the Zettler note!
