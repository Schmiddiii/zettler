---
title: Zettler
---

Zettler is a language server for working managing zettelkasten, personal wikis, journals or other forms of knowledge bases.

__Note to GitLab Viewers__: GitLab interprets the links in this folder as links to the GitLab wiki.
This wiki is unused, instead all documentation can be found in this `doc/`-folder.

This documentation has three main purposes:

- User documentation for getting started with Zettler.
- Developer documentation to modify Zettler to fit your workflow.
- An example directory which can directly be used to try out Zettler without installing anything globally: [[Try it Out Without Installing]]

# Features

This section lists main features of Zettler.
To inspect any feature closer, just use the "go to definition" menu item/keyboard shortcut over any of the list items:

- [[Go to Definition]]
- [[Hover]]
- [[Completion]]
- [[List Notes]]
- [[Create Note]]
- [[Renaming Notes]]

# Configuration

See [[Configuration]] for notes on how to configure Zettler.
Note that Zettler was not intended to be massively configurable, instead it should be easy to extend the code itself to implement features personalized to your zettelkasten.

# Editors

Some notes about usage in editors:

- [[Helix]]

# Limitations

The [[Limitations]] note contains information about limitations, which may negatively influence performance of Zettler, contains editor-specific bugs we cannot work around, or possibly incorrect behavior that were not worth fixing yet.

# Similar Projects

The [[Similar Projects]] note contains information about related projects which inspired this one, and why I felt like those were not suitable for my needs.
