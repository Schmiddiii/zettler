---
title: Similar Projects
tags: []
---

This document provides links to similar projects, which provided inspiration for this project.
It also contains explanations about differences to them and the reasons why chose to not use them.

# [telekasten.nvim](https://github.com/renerocksai/telekasten.nvim)

This is a great project I have been using since ages.
But as you can guess by the name, it only supports neovim for now.
As I started using [[Helix]] more, to the point that `telekasten.nvim` was the only reason I kept using neovim, I wanted to switch away from it.

# [zk](https://github.com/zk-org/zk)

This is a project which allows usage of the Zettelkasten via the LSP.
Nevertheless, I felt like the LSP was a "second-class" feature of it, missing major features like creating notes.
Furthermore, not being able to rename files seemed like a big issue for me personally.
Trying to import my Zettelkasten into `zk` also failed, as I was not careful with my frontmatter, e.g. some titles included `:` without proper quoting, resulting in errors.
There was no easy fix for this as far as I saw.
