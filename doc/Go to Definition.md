---
title: Go to Definition
tags: [feature]
---

You can directly go to referenced notes in your knowledge base.
You can e.g. use the following link to jump back to the main note using this link:

[[Zettler]]

Please also inspect the [[Link Styles]] currently allowed.
