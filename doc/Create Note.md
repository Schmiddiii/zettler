---
title: Create Note
tags: [feature]
---

You can create using "lsp workspace commands".
This requires templates in your zettelkasten, under the `templates`-folder.

By default, three templates are templates are supported:

- `new_note.md`: General new note.
- `daily.md`: Daily journal.
- `weekly.md`: Weekly journal.

Note you can also use templating variables, as described in [[Templating]].
Furthermore, note that the filename is by default specified the same as in [[Renaming Notes]].

Feel free to try it out by creating some new note running the workspace command `new-note` (in [[Helix]] via `:lsp-workspace-command new-note`).

This is intended to be manually extended by the user, by adding the command to `COMMANDS` in `src/commands.rs`.
