---
title: Try it Out Without Installing
tags: []
---

You can directly try out [[Zettler]] without installing it in this directory and using the [[Helix]] editor.

For that, just compile Zettler using:

```sh
cargo build
```

Now just edit some file inside this directory and Zettler should work, e.g.:


```sh
hx doc/Zettler.md
```

This works via the project-specific `.helix/languages.toml`-file.
