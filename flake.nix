{
  description = "LSP for my Zettelkasten";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils, ... }@inputs:
    (flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = import nixpkgs {
            inherit system;
          };
          name = "zettler";
        in
        { 
          packages.default = 
            with pkgs;
            stdenv.mkDerivation rec {
              cargoDeps = rustPlatform.importCargoLock {
                lockFile = ./Cargo.lock;
              };
              src = let fs = lib.fileset; in fs.toSource {
                root = ./.;
                fileset =
                  fs.difference
                    ./.
                    (fs.unions [
                      (fs.maybeMissing ./result)
                      ./flake.nix
                      ./flake.lock
                    ]);
              };
              buildInputs = [ ];
              nativeBuildInputs = [ pkgs.rustPlatform.cargoSetupHook pkgs.rustc pkgs.cargo ];

              inherit name;
            };
          devShells.default =
            let 
              run = pkgs.writeShellScriptBin "run" ''
                 export PATH=$PATH:./target/debug/
                 cargo build && hx $@
              '';
            in
            pkgs.mkShell {
              src = ./.;
              buildInputs = [ ];
              nativeBuildInputs = [ pkgs.rustPlatform.cargoSetupHook pkgs.rustc pkgs.cargo pkgs.helix ] ++ [ run ];
            };
        })
    );
}
