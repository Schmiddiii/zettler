# Zettler

A language server for working managing zettelkasten, personal wikis, journals or other forms of knowledge bases.

While Zettler should work with any editor supporting LSP, it was primarily developed and tested with [Helix](https://helix-editor.com/).

## More Information

For more information on features, configurations, limitations and similar projects, look at the [doc folder](https://gitlab.com/schmiddiii/zettler/-/tree/master/doc/Zettler.md?ref_type=heads).
This can also be used to directly try out Zettler without global installation, just compiling it.
