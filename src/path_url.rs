use std::{
    ffi::OsStr,
    fmt::{Debug, Display},
    hash::Hash,
    path::{Path, PathBuf},
};

use tower_lsp::lsp_types::Url;

use crate::error::Error;

/// A struct which is guaranteed to be convertible to a [Url] and [PathBuf].
///
/// Note that this holds both the Url an the PathBuf at once using slightly more memory, but which allows implementation of AsRef.
#[derive(Clone)]
pub struct PathUrl(Url, PathBuf);

impl TryFrom<PathBuf> for PathUrl {
    type Error = Error;

    fn try_from(value: PathBuf) -> Result<Self, Self::Error> {
        Url::from_file_path(&value)
            .map(|u| Self(u, value))
            .map_err(|_| Error::PathCannotBeUri)
    }
}

impl TryFrom<Url> for PathUrl {
    type Error = Error;

    fn try_from(value: Url) -> Result<Self, Self::Error> {
        if let Ok(path) = value.to_file_path() {
            Ok(Self(value, path))
        } else {
            Err(Error::UriCannotBePath)
        }
    }
}

impl AsRef<Path> for PathUrl {
    fn as_ref(&self) -> &Path {
        self.1.as_path()
    }
}

impl From<PathUrl> for Url {
    fn from(value: PathUrl) -> Self {
        value.0
    }
}

impl From<PathUrl> for PathBuf {
    fn from(value: PathUrl) -> Self {
        value.1
    }
}

impl Hash for PathUrl {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        // Note: Only hash the URL, the PathBuf points to the same place.
        self.0.hash(state)
    }
}

impl PartialEq for PathUrl {
    fn eq(&self, other: &Self) -> bool {
        self.0.eq(&other.0)
    }
}

impl Eq for PathUrl {}

impl PathUrl {
    pub fn push<P: AsRef<Path>>(&mut self, path: P) {
        let path = path.as_ref();
        self.0
            .path_segments_mut()
            .expect("file path cannot-be-base")
            .extend(path.iter().map(|s| s.to_string_lossy()));
        self.1.push(path);
    }

    pub fn join<P: AsRef<Path>>(&self, path: P) -> Self {
        let mut ret = self.clone();
        ret.push(path);
        ret
    }

    pub fn exists(&self) -> bool {
        self.1.exists()
    }

    pub fn contains_component<S: AsRef<OsStr>>(&self, component: S) -> bool {
        self.1
            .components()
            .any(|c| c.as_os_str() == component.as_ref())
    }
}

impl Debug for PathUrl {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.0)
    }
}

impl Display for PathUrl {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl PartialOrd for PathUrl {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for PathUrl {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.0.cmp(&other.0)
    }
}
