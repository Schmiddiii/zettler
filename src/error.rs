use tower_lsp::jsonrpc::Error as LspError;

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("io error")]
    Io(#[from] std::io::Error),
    #[error("uri is cannot-be-a-base")]
    UriCannotbeBase,
    #[error("only file uri is supported")]
    OnlySupportsFile,
    #[error("uri does not correspond to a path")]
    UriIsNoPath,
    #[error("only utf-8 is supported")]
    OnlySupportsUtf8,
    #[error("no zettelkasten path was configured")]
    NoPathConfigured,
    #[error("lsp error")]
    LspError(#[from] LspError),
    #[error("the path cannot be an ui")]
    PathCannotBeUri,
    #[error("the uri cannot be a path")]
    UriCannotBePath,
}

impl From<Error> for LspError {
    fn from(value: Error) -> Self {
        tracing::error!(%value);
        match value {
            Error::LspError(e) => e,
            _ => Self::internal_error(),
        }
    }
}
