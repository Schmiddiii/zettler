mod command;
mod error;
mod file_list_watcher;
mod file_naming_rule;
mod path_url;
mod templating;
mod utils;
use crate::command::COMMANDS;
use crate::error::Error;
use crate::file_list_watcher::FileListWatcher;
use crate::file_naming_rule::*;
use crate::path_url::PathUrl;
use crate::templating::Templating;
use crate::utils::*;

use std::borrow::Cow;
use std::collections::HashMap;
use std::fs::File;
use std::io::{Read, Write};
use std::path::{Path, PathBuf};
use std::sync::{Arc, Mutex, MutexGuard};

use nucleo_matcher::pattern::{AtomKind, CaseMatching, Normalization};
use nucleo_matcher::Utf32Str;
use tower_lsp::jsonrpc::Error as LspError;
use tower_lsp::lsp_types::*;
use tower_lsp::{Client, LanguageServer, LspService, Server};
use tracing::{debug, info, instrument, warn};

const ZETTELKASTEN_PATH: &str = "zettelkasten-path";
pub(crate) const FILE_EXTENSION: &str = "md";

const TEMPLATE_SUBDIR: &str = "templates";

#[derive(Debug)]
struct BackendInner {
    // Maps the URI of the document to the current version and its contents.
    opened_documents: HashMap<PathUrl, (i32, String)>,

    zettelkasten_path: Option<PathUrl>,

    file_list_watcher: Option<FileListWatcher>,

    fuzzy_matcher: nucleo_matcher::Matcher,
}

impl BackendInner {
    fn list_notes(&self) -> Vec<PathUrl> {
        self.file_list_watcher
            .as_ref()
            .into_iter()
            .flat_map(|w| w.files())
            .collect()
    }

    fn zettelkasten_path(&self) -> Result<&PathUrl, Error> {
        self.zettelkasten_path
            .as_ref()
            .ok_or(Error::NoPathConfigured)
    }

    fn format_path<P: AsRef<Path>>(&self, path: P) -> String {
        let mut zettelkasten_path = self
            .zettelkasten_path()
            .map(|p| {
                PathBuf::from(p.clone())
                    .into_os_string()
                    .to_string_lossy()
                    .to_string()
            })
            .unwrap_or_default();
        zettelkasten_path.push('/');

        let path_string = path
            .as_ref()
            .to_owned()
            .into_os_string()
            .to_string_lossy()
            .to_string();

        // Strip prefix
        let path_string = if let Some(stripped) = path_string.strip_prefix(&*zettelkasten_path) {
            stripped.to_string()
        } else {
            path_string
        };
        // Strip suffix
        let path_string =
            if let Some(stripped) = path_string.strip_suffix(&(".".to_string() + FILE_EXTENSION)) {
                stripped.to_string()
            } else {
                path_string
            };

        path_string
    }

    #[instrument(skip(self), fields(zettelkasten = %self.zettelkasten_path.as_ref().map(|d| d.to_string()).unwrap_or_default(), uri = %uri), ret)]
    fn document_is_in_zettelkasten(&self, uri: &PathUrl) -> bool {
        self.zettelkasten_path
            .as_ref()
            .is_some_and(|z| PathBuf::from(uri.clone()).starts_with(z))
    }

    fn update_document<S: AsRef<str>>(&mut self, uri: &PathUrl, version: i32, text: S) {
        if !self.document_is_in_zettelkasten(uri) {
            debug!("Asked to update a document that is not stored in the zettelkasten; ignoring");
            return;
        }
        if let Some((old_version, _)) = self.opened_documents.get(uri) {
            if version < *old_version {
                warn!("In-memory document is newer than sent document; doing nothing");
                return;
            }
        }

        self.opened_documents
            .insert(uri.clone(), (version, text.as_ref().to_owned()));
    }

    fn cached_document(&self, uri: &PathUrl) -> Option<&(i32, String)> {
        self.opened_documents.get(uri)
    }

    fn frontmatter(&self, uri: &PathUrl) -> Option<serde_yaml::Value> {
        frontmatter_for_string(&self.cached_document(uri)?.1)
    }

    fn document(&self, uri: &PathUrl) -> Result<Cow<'_, str>, Error> {
        if let Some(cached) = self.cached_document(uri) {
            Ok((&cached.1).into())
        } else {
            content_for_uri(uri).map(|s| s.into())
        }
    }

    fn reference_at_position(&self, uri: &PathUrl, position: &Position) -> Option<&str> {
        self.reference_at_position_with_range(uri, position)
            .map(|r| r.0)
    }

    #[instrument(skip(self, uri), fields(uri = %uri), ret)]
    fn reference_at_position_with_range(
        &self,
        uri: &PathUrl,
        position: &Position,
    ) -> Option<(&str, Range)> {
        info!("Querying reference at position");
        // Note: Position is guaranteed to be UTF-8 here as we required that in the server capabilities.

        let document = self.cached_document(uri).as_ref().map(|d| &d.1);
        if let Some(document) = document {
            let Some(line) = document.lines().skip(position.line as usize).next() else {
                warn!("Querying reference at a position where no line is; this should not be possible");
                return None;
            };
            let Some(index) = line
                .char_indices()
                .skip(position.character as usize)
                .next()
                .map(|(i, _c)| i)
            else {
                warn!("Querying reference at a character where no line is; this should not be possible");
                return None;
            };

            // Note: This should never panic; `char_indices` only returns valid char boundaries and we shift by a valid amount.
            let (before, after) = line.split_at(index);

            let (open_pos, close_pos) = if before.ends_with(']') && after.starts_with(']') {
                (before.rfind("[[").map(|p| p + 2), Some(before.len() - 1))
            } else if after.starts_with("[[") {
                (
                    Some(before.len() + 2),
                    after.find("]]").map(|p| p + before.len()),
                )
            } else if after.starts_with('[') && before.ends_with('[') {
                (
                    Some(before.len() + 1),
                    after.find("]]").map(|p| p + before.len()),
                )
            } else {
                (
                    before.rfind("[[").map(|p| p + 2),
                    after.find("]]").map(|p| p + before.len()),
                )
            };

            let Some(open_pos) = open_pos else {
                debug!("Could not find any [[ before the position; there is no reference there",);
                return None;
            };

            let Some(close_pos) = close_pos else {
                debug!("Could not find any ]] after the position; there is no reference there",);
                return None;
            };

            // This should not panic; open_pos and close_pos are valid char indices.
            let between = &line[open_pos..close_pos];

            if between.contains("[[") || between.contains("]]") {
                debug!("There were other [[ or ]] between those found; the position is not at a reference to another document");
                return None;
            }

            Some((
                between,
                Range::new(
                    // Include brackets.
                    Position::new(position.line, open_pos as u32 - 2),
                    Position::new(position.line, close_pos as u32 + 2),
                ),
            ))
        } else {
            warn!("Querying reference at position that is not opened; this should not be possible");
            None
        }
    }

    fn note_by_reference<S: AsRef<str>>(&self, reference: S) -> Result<PathUrl, Error> {
        self.zettelkasten_path()
            .map(|p| p.join(&format!("{}.{}", reference.as_ref(), FILE_EXTENSION)))
    }

    fn replace_in_zettelkasten<S: AsRef<str>>(
        &self,
        needle: S,
        replacement: S,
    ) -> Result<Vec<TextDocumentEdit>, Error> {
        let mut edits = vec![];
        let needle = needle.as_ref();
        let replacement = replacement.as_ref();

        for doc in self.list_notes() {
            if let Some(cached) = self.opened_documents.get(&doc) {
                let text_document = OptionalVersionedTextDocumentIdentifier {
                    uri: doc.clone().into(),
                    version: Some(cached.0),
                };
                let cached = &cached.1;
                let positions = find_in_string(&cached[..], needle);

                edits.push(TextDocumentEdit {
                    text_document,
                    edits: positions
                        .into_iter()
                        .map(|p| {
                            OneOf::Left(TextEdit {
                                range: p,
                                new_text: replacement.to_string(),
                            })
                        })
                        .collect(),
                });
            } else {
                replace_in_file(&doc, needle, replacement)?;
            }
        }

        Ok(edits)
    }

    fn search_in_zettelkasten<S: AsRef<str>>(&self, needle: S) -> Result<Vec<Location>, Error> {
        let mut results = vec![];
        let needle = needle.as_ref();

        for doc in self.list_notes() {
            let ranges = if let Some((_version, cached)) = self.opened_documents.get(&doc) {
                find_in_string(&cached[..], needle)
            } else {
                find_in_string(&content_for_uri(&doc)?[..], needle)
            };

            let locations = ranges.into_iter().map(|r| Location {
                uri: doc.clone().into(),
                range: r,
            });
            results.extend(locations);
        }

        Ok(results)
    }
}

#[derive(Debug)]
struct Backend {
    client: Client,

    inner: Arc<Mutex<BackendInner>>,
}

impl Backend {
    fn new(client: Client) -> Self {
        Self {
            client,
            inner: Arc::new(Mutex::new(BackendInner {
                opened_documents: Default::default(),
                zettelkasten_path: None,
                file_list_watcher: None,
                fuzzy_matcher: nucleo_matcher::Matcher::new(nucleo_matcher::Config::DEFAULT),
            })),
        }
    }

    fn inner(&self) -> MutexGuard<BackendInner> {
        self.inner.lock().expect("Backend inner poisoned")
    }

    async fn execute_new_note<P: AsRef<Path>>(&self, template_path: P) -> Result<(), Error> {
        let mut file = File::open(template_path)?;
        let mut content = String::new();
        file.read_to_string(&mut content)?;

        let mut content = Templating::template(&content);
        let cursor = Templating::cursor(&mut content);

        let frontmatter = frontmatter_for_string(&content);
        let destination_path = frontmatter
            .and_then(|f| self.filename_for_frontmatter(f).ok().flatten())
            .ok_or(Error::NoPathConfigured)?;

        if !destination_path.exists() {
            let mut file = File::options()
                .read(true)
                .write(true)
                .create_new(true)
                .open(&destination_path)?;
            file.write(content.as_bytes())?;
        }

        let selection = if let Some(position) = cursor {
            Some(Range::new(position.clone(), position))
        } else {
            None
        };

        self.client
            .show_document(ShowDocumentParams {
                // TODO: Better error
                uri: Url::from_file_path(destination_path).map_err(|_| Error::NoPathConfigured)?,
                external: None,
                take_focus: Some(true),
                selection,
            })
            .await?;

        Ok(())
    }

    async fn new_note_from_template<P: AsRef<Path>>(&self, template: P) -> Result<(), Error> {
        let mut template_path = self.inner().zettelkasten_path()?.clone();
        template_path.push(TEMPLATE_SUBDIR);
        template_path.push(template);

        self.execute_new_note(&template_path).await
    }

    #[instrument(skip_all)]
    fn filename_for_frontmatter(
        &self,
        frontmatter: serde_yaml::Value,
    ) -> Result<Option<PathUrl>, Error> {
        let inner = self.inner();
        let zettelkasten_path = inner.zettelkasten_path()?;

        let Some(generated_relative_path) =
            apply_first_file_naming_rule(NAMING_RULES.as_slice(), &frontmatter)
        else {
            return Ok(None);
        };

        Ok(Some(zettelkasten_path.join(generated_relative_path)))
    }
}

#[tower_lsp::async_trait]
impl LanguageServer for Backend {
    #[instrument(skip_all, fields(process = params.process_id, workspace = ?params.workspace_folders))]
    async fn initialize(&self, params: InitializeParams) -> Result<InitializeResult, LspError> {
        info!("Initializing zettler");

        let client_capabilities = &params.capabilities;
        let general_client_capabilities = &client_capabilities.general;
        let client_encodings = general_client_capabilities
            .as_ref()
            .and_then(|c| c.position_encodings.as_ref());
        let supports_utf8 =
            client_encodings.is_some_and(|c| c.contains(&PositionEncodingKind::UTF8));

        if !supports_utf8 {
            return Err(Error::OnlySupportsUtf8.into());
        }

        let Some(zettelkasten_path): Option<PathUrl> = params
            .initialization_options
            .as_ref()
            .and_then(|v| v.as_object())
            .and_then(|m| m.get(ZETTELKASTEN_PATH))
            .and_then(|p| p.as_str())
            .map(PathBuf::from)
            .and_then(|p| p.canonicalize().ok())
            .and_then(|p| p.try_into().ok())
        else {
            return Err(Error::NoPathConfigured.into());
        };

        info!("Zettelkasten path: {}", zettelkasten_path);
        let mut inner = self.inner();
        let watcher = FileListWatcher::new(zettelkasten_path.clone());
        watcher.run()?;
        inner.zettelkasten_path = Some(zettelkasten_path);
        inner.file_list_watcher = Some(watcher);

        Ok(InitializeResult {
            capabilities: ServerCapabilities {
                hover_provider: Some(HoverProviderCapability::Simple(true)),
                definition_provider: Some(OneOf::Left(true)),
                workspace_symbol_provider: Some(OneOf::Left(true)),
                completion_provider: Some(CompletionOptions {
                    trigger_characters: Some(vec!["[".to_string()]),
                    ..Default::default()
                }),
                references_provider: Some(OneOf::Left(true)),

                // Always send full file; this is wastefull, but prevents me of needing to implement update-logic server-side.
                // TODO: Support partial sync. Maybe borrow <https://github.com/rust-lang/rust-analyzer/blob/ba329913fa33c29d4ccabf46998d3a0cfac57b0c/crates/rust-analyzer/src/lsp_utils.rs#L131>.
                text_document_sync: Some(TextDocumentSyncCapability::Options(
                    TextDocumentSyncOptions {
                        open_close: Some(true),
                        change: Some(TextDocumentSyncKind::FULL),
                        save: Some(TextDocumentSyncSaveOptions::Supported(true)),
                        ..Default::default()
                    },
                )),

                execute_command_provider: Some(ExecuteCommandOptions {
                    commands: COMMANDS.keys().map(|s| s.to_string()).collect(),
                    ..Default::default()
                }),
                ..Default::default()
            },
            ..Default::default()
        })
    }

    #[instrument(skip_all)]
    async fn initialized(&self, _params: InitializedParams) {
        info!("Initialized zettler");
    }

    #[instrument(skip_all)]
    async fn shutdown(&self) -> Result<(), LspError> {
        info!("Shutting down zettler");
        Ok(())
    }

    #[instrument(skip_all, fields(uri = %params.text_document.uri, version = params.text_document.version))]
    async fn did_open(&self, params: DidOpenTextDocumentParams) {
        info!("Opening document");
        let mut inner = self.inner();
        let doc = &params.text_document;
        let Ok(uri) = doc.uri.clone().try_into() else {
            tracing::warn!("Got non-file uri; ignoring");
            return;
        };

        if !inner.document_is_in_zettelkasten(&uri) {
            debug!("Opened a document that is not in the zettelkasten; ignoring");
            return;
        }

        let version = doc.version;

        inner.update_document(&uri, version, &doc.text);
    }

    #[instrument(skip_all, fields(uri = %params.text_document.uri))]
    async fn did_save(&self, params: DidSaveTextDocumentParams) {
        info!("Saving file");
        let Ok(uri): Result<PathUrl, _> = params.text_document.uri.clone().try_into() else {
            tracing::warn!("Got non-file uri; ignoring");
            return;
        };

        if uri.contains_component(TEMPLATE_SUBDIR) {
            tracing::trace!("Saving template file; will not rename");
            return;
        }

        let Some(frontmatter) = self.inner().frontmatter(&uri) else {
            return;
        };

        let Some(new_uri) = self.filename_for_frontmatter(frontmatter).ok().flatten() else {
            return;
        };

        if new_uri != uri && !new_uri.exists() {
            tracing::info!("Will rename: {} -> {}", uri, new_uri);

            // First, rename references in all the files.
            // This is important to do first, otherwise the list of notes in the zettelkasten would be outdated when renaming the file itself.
            let result = {
                let inner = self.inner();

                inner.replace_in_zettelkasten(
                    format!("[[{}]]", inner.format_path(&uri)),
                    format!("[[{}]]", inner.format_path(&new_uri)),
                )
            };

            if let Err(e) = result {
                tracing::error!(message = "Failed to replace references in other files", error = %e);
                return;
            }

            let result = result.unwrap();

            let _ = self
                .client
                .apply_edit(WorkspaceEdit {
                    changes: None,
                    document_changes: Some(DocumentChanges::Edits(result)),
                    change_annotations: None,
                })
                .await;

            // Secondly, rename the file itself via a workspace edit.
            let result = self
                .client
                .apply_edit(WorkspaceEdit {
                    document_changes: Some(DocumentChanges::Operations(vec![
                        DocumentChangeOperation::Op(ResourceOp::Rename(RenameFile {
                            old_uri: uri.clone().into(),
                            new_uri: new_uri.clone().into(),
                            options: None,
                            annotation_id: None,
                        })),
                    ])),
                    ..Default::default()
                })
                .await;

            if let Err(e) = result {
                tracing::error!(message = "Failed to rename file", error = %e);
                return;
            }
        }
    }

    #[instrument(skip_all, fields(uri = %params.text_document.uri, version = params.text_document.version))]
    async fn did_change(&self, params: DidChangeTextDocumentParams) {
        info!("Changing document");
        let mut inner = self.inner();
        let doc = &params.text_document;
        let Ok(uri) = &doc.uri.clone().try_into() else {
            tracing::warn!("Got non-file uri; ignoring");
            return;
        };

        if !inner.document_is_in_zettelkasten(&uri) {
            debug!("Changed a document that is not in the zettelkasten; ignoring");
            return;
        }

        let version = doc.version;

        for change in params.content_changes {
            let range = &change.range;
            let text = &change.text;

            if range.is_some() {
                warn!("Client requested document change in range; this is not yet supported by zettler; doing nothing");
            }

            inner.update_document(&uri, version, text);
        }
    }

    #[instrument(skip_all, fields(uri = %params.text_document.uri))]
    async fn did_close(&self, params: DidCloseTextDocumentParams) {
        info!("Closing document");
        let mut inner = self.inner();
        let doc = &params.text_document;
        let Ok(uri) = &doc.uri.clone().try_into() else {
            tracing::warn!("Got non-file uri; ignoring");
            return;
        };

        if !inner.document_is_in_zettelkasten(&uri) {
            debug!("Closed a document that is not in the zettelkasten; ignoring");
            return;
        }

        inner.opened_documents.remove(uri);
    }

    /// On hover, show the content of the hovered file if there is some available.
    #[instrument(skip_all, fields(uri = %params.text_document_position_params.text_document.uri, position = ?params.text_document_position_params.position))]
    async fn hover(&self, params: HoverParams) -> Result<Option<Hover>, LspError> {
        info!("Hover");
        let inner = self.inner();
        let uri = params
            .text_document_position_params
            .text_document
            .uri
            .try_into()?;

        if !inner.document_is_in_zettelkasten(&uri) {
            debug!("Hovered in a document that is not in the zettelkasten; ignoring");
            return Ok(None);
        }

        let position = &params.text_document_position_params.position;
        let reference = inner.reference_at_position(&uri, position);
        if let Some(reference) = reference {
            let updated_uri = inner.note_by_reference(reference)?;

            let content = inner.document(&updated_uri)?;

            let firstline = extract_firstline_for_file(&content);

            // TODO: Possibly add range
            Ok(Some(Hover {
                contents: HoverContents::Scalar(MarkedString::from_markdown(firstline)),
                range: None,
            }))
        } else {
            Ok(None)
        }
    }

    // On "go to definition", jump to the file that is currently referenced by the position of the cursor.
    #[instrument(skip_all, fields(uri = %params.text_document_position_params.text_document.uri, position = ?params.text_document_position_params.position))]
    async fn goto_definition(
        &self,
        params: GotoDefinitionParams,
    ) -> Result<Option<GotoDefinitionResponse>, LspError> {
        info!("Go to definition");
        let inner = self.inner();
        let uri = params
            .text_document_position_params
            .text_document
            .uri
            .try_into()?;

        if !inner.document_is_in_zettelkasten(&uri) {
            debug!("Goto-definition in a document that is not in the zettelkasten; ignoring");
            return Ok(None);
        }

        let position = &params.text_document_position_params.position;
        let reference = inner.reference_at_position(&uri, position);

        if let Some(reference) = reference {
            let updated_uri = inner.note_by_reference(reference)?;

            // TODO: Point position to something?
            Ok(Some(GotoDefinitionResponse::Scalar(Location {
                uri: updated_uri.into(),
                range: Range {
                    start: Position::new(0, 0),
                    end: Position::new(0, 0),
                },
            })))
        } else {
            Ok(None)
        }
    }

    /// Listing all symbols list the notes in the zettelkasten.
    #[instrument(skip_all, fields(query = params.query))]
    async fn symbol(
        &self,
        params: WorkspaceSymbolParams,
    ) -> Result<Option<Vec<SymbolInformation>>, LspError> {
        info!("Workspace Symbols");
        let mut inner = self.inner();

        let mut buf = Vec::new();
        let pattern = nucleo_matcher::pattern::Pattern::new(
            &params.query,
            CaseMatching::Ignore,
            Normalization::Smart,
            AtomKind::Fuzzy,
        );

        let mut notes: Vec<_> = inner
            .list_notes()
            .into_iter()
            .map(|p| {
                let path = inner.format_path(&p);
                let score = pattern.score(Utf32Str::new(&path, &mut buf), &mut inner.fuzzy_matcher);
                (Url::from_file_path(p.clone()), path, score)
            })
            .filter(|(_, _, p)| p.is_some())
            .filter_map(|(u, s, p)| if let Ok(u) = u { Some((u, s, p)) } else { None })
            .collect();

        notes.sort_by_key(|(_, _, p)| std::cmp::Reverse(*p));

        // The field `deprecated` is deprecated (funny), but nevertheless required to construct `SymbolInformation`.
        #[allow(deprecated)]
        let symbols = notes
            .into_iter()
            .map(|(p, s, _)| SymbolInformation {
                name: s,
                kind: SymbolKind::FILE,
                tags: None,
                deprecated: None,
                // TODO: Maybe not at the start of the file?
                location: Location {
                    uri: p,
                    range: Range::new(Position::new(0, 0), Position::new(0, 0)),
                },
                container_name: None,
            })
            .collect();

        Ok(Some(symbols))
    }

    #[instrument(skip_all, fields(uri = %params.text_document_position.text_document.uri, position = ?params.text_document_position.position))]
    async fn completion(
        &self,
        params: CompletionParams,
    ) -> Result<Option<CompletionResponse>, LspError> {
        tracing::info!("Completion");
        let inner = self.inner();
        let uri = &params.text_document_position.text_document.uri.try_into()?;
        let position = &params.text_document_position.position;

        // Note: This assumes the closing brackets also exist.
        // This is the case if the editor automatically inserts them when inserting the opening bracket.
        let reference = inner
            .reference_at_position_with_range(uri, position)
            .map(|(s, r)| (s.to_ascii_lowercase(), r));

        if let Some(reference) = reference {
            let notes: Vec<_> = inner
                .list_notes()
                .into_iter()
                .map(|p| inner.format_path(p))
                .collect();

            // Leave sorting and filtering to the client.

            let items = notes.into_iter().map(|m| CompletionItem {
                label: m.clone(),
                kind: Some(CompletionItemKind::SNIPPET),
                text_edit: Some(CompletionTextEdit::Edit(TextEdit {
                    range: reference.1,
                    new_text: format!("[[{}]]$0", m),
                })),
                ..Default::default()
            });
            Ok(Some(CompletionResponse::Array(items.collect())))
        } else {
            Ok(None)
        }
    }

    #[instrument(skip_all, fields(command = %params.command))]
    async fn execute_command(
        &self,
        params: ExecuteCommandParams,
    ) -> Result<Option<serde_json::Value>, LspError> {
        tracing::info!("Executing command");

        let command = params.command;

        if let Some(cmd) = COMMANDS.get(&command[..]) {
            cmd.run(&self).await?
        } else {
            tracing::warn!("unknown command");
        }

        Ok(None)
    }

    // On "go to references", list all file linking to the current file (the position in the current file is of no interest).
    #[instrument(skip_all, fields(uri = %params.text_document_position.text_document.uri))]
    async fn references(&self, params: ReferenceParams) -> Result<Option<Vec<Location>>, LspError> {
        info!("Go to references");
        let inner = self.inner();
        let uri: PathUrl = params.text_document_position.text_document.uri.try_into()?;
        let format = format!("[[{}]]", inner.format_path(uri));
        let locations = inner.search_in_zettelkasten(&format)?;

        Ok(Some(locations))
    }
}

#[tokio::main]
async fn main() {
    use tracing_appender::rolling::{RollingFileAppender, Rotation};
    use tracing_subscriber::{self, prelude::*, EnvFilter};

    let fmt = if std::env::var("ZETTLER_LOG_FILE").is_ok() {
        let file_appender = RollingFileAppender::new(Rotation::DAILY, ".", "zettler.log");
        let (non_blocking, _guard) = tracing_appender::non_blocking(file_appender);

        tracing_subscriber::fmt::layer()
            .with_ansi(false)
            .with_writer(non_blocking)
            .boxed()
    } else {
        tracing_subscriber::fmt::layer()
            .with_ansi(false)
            .with_writer(std::io::stderr)
            .boxed()
    };

    let filter =
        EnvFilter::try_from_default_env().unwrap_or_else(|_| EnvFilter::new("error,zettler=trace"));

    tracing_subscriber::registry().with(filter).with(fmt).init();
    tracing::info!("Set up tracing");

    let stdin = tokio::io::stdin();
    let stdout = tokio::io::stdout();

    let (service, socket) = LspService::new(Backend::new);
    Server::new(stdin, stdout, socket).serve(service).await;
}
