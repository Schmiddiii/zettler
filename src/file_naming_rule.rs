use std::path::{Path, PathBuf};

use serde_yaml::Value;

use crate::FILE_EXTENSION;

/// Default file name when no other rule matches.
const DEFAULT_NEW_NOTE_FILE_NAME: &str = "unnamed.md";
/// The subdirectories to use for daily and weekly notes.
const DAILY_SUBDIR: &str = "daily";
const WEEKLY_SUBDIR: &str = "weekly";

lazy_static::lazy_static! {
    /// All available file naming rules.
    pub static ref NAMING_RULES: Vec<Box<dyn FileNamingRule + Send + Sync + 'static>> = vec![
        Box::new(DailyNaming),
        Box::new(WeeklyNaming),
        Box::new(IdTitleNaming),
        Box::new(TitleNaming),
        Box::new(IdNaming),
        Box::new(ConstantFileNaming::new(DEFAULT_NEW_NOTE_FILE_NAME))
    ];
}

/// Apply the first file naming rule that is possible to apply.
pub fn apply_first_file_naming_rule(
    rules: &[Box<dyn FileNamingRule + Send + Sync + 'static>],
    frontmatter: &Value,
) -> Option<PathBuf> {
    rules.into_iter().flat_map(|r| r.name(frontmatter)).next()
}

/// How to name files based on their frontmatter.
pub trait FileNamingRule {
    /// Suggest how to name a file with the given frontmatter.
    ///
    /// If the rule does not apply, return `None`. Otherwise return a path relative to the zettelkasten directory.
    fn name(&self, frontmatter: &Value) -> Option<PathBuf>;
}

// The remainder of the file are implementations of file naming rules, ordered from most generic to most specific.

/// Give the file a constant name, disregarding the frontmatter.
pub struct ConstantFileNaming(PathBuf);

impl ConstantFileNaming {
    pub fn new<P: AsRef<Path>>(path: P) -> Self {
        Self(path.as_ref().to_owned())
    }
}

impl FileNamingRule for ConstantFileNaming {
    fn name(&self, _frontmatter: &Value) -> Option<PathBuf> {
        Some(self.0.clone())
    }
}

/// Give the file the name `<id>.md` based on its id.
pub struct IdNaming;

impl FileNamingRule for IdNaming {
    fn name(&self, frontmatter: &Value) -> Option<PathBuf> {
        // Allow both int and string IDs.
        let id = frontmatter.get("id");
        let id = id
            .and_then(|v| v.as_i64().map(|i| i.to_string()))
            .or_else(|| id.and_then(|v| v.as_str()).map(|s| s.to_string()));

        if let Some(id) = id {
            Some(PathBuf::from(&format!("{}.{}", id, FILE_EXTENSION)))
        } else {
            None
        }
    }
}

/// Give the file the name `<title>.md` based on its title.
pub struct TitleNaming;

impl FileNamingRule for TitleNaming {
    fn name(&self, frontmatter: &Value) -> Option<PathBuf> {
        let title = frontmatter.get("title").and_then(|v| v.as_str());

        if let Some(title) = title {
            Some(PathBuf::from(&format!("{}.{}", title, FILE_EXTENSION)))
        } else {
            None
        }
    }
}

/// Give the file the name `<id>-<title>.md` based on its id and title.
pub struct IdTitleNaming;

impl FileNamingRule for IdTitleNaming {
    fn name(&self, frontmatter: &Value) -> Option<PathBuf> {
        // Allow both int and string IDs.
        let id = frontmatter.get("id");
        let id = id
            .and_then(|v| v.as_i64().map(|i| i.to_string()))
            .or_else(|| id.and_then(|v| v.as_str()).map(|s| s.to_string()));

        let title = frontmatter.get("title").and_then(|v| v.as_str());

        if let (Some(id), Some(title)) = (id, title) {
            Some(PathBuf::from(&format!(
                "{}-{}.{}",
                id, title, FILE_EXTENSION
            )))
        } else {
            None
        }
    }
}

/// Give the file the name `daily/<date>.md` based on its date and if it has the tag `daily`.
pub struct DailyNaming;

impl FileNamingRule for DailyNaming {
    fn name(&self, frontmatter: &Value) -> Option<PathBuf> {
        let tags: Vec<&str> = frontmatter
            .get("tags")
            .and_then(|v| v.as_sequence())
            .into_iter()
            .flatten()
            .filter_map(|v| v.as_str())
            .collect();

        let date = frontmatter.get("date").and_then(|v| v.as_str());

        if let (true, Some(date)) = (tags.contains(&"daily"), date) {
            Some(PathBuf::from(&format!(
                "{}/{}.{}",
                DAILY_SUBDIR, date, FILE_EXTENSION
            )))
        } else {
            None
        }
    }
}

/// Give the file the name `weekly/title.md` based on its date and if it has the tag `weekly`.
pub struct WeeklyNaming;

impl FileNamingRule for WeeklyNaming {
    fn name(&self, frontmatter: &Value) -> Option<PathBuf> {
        let tags: Vec<&str> = frontmatter
            .get("tags")
            .and_then(|v| v.as_sequence())
            .into_iter()
            .flatten()
            .filter_map(|v| v.as_str())
            .collect();

        let title = frontmatter.get("title").and_then(|v| v.as_str());

        if let (true, Some(title)) = (tags.contains(&"weekly"), title) {
            Some(PathBuf::from(&format!(
                "{}/{}.{}",
                WEEKLY_SUBDIR, title, FILE_EXTENSION
            )))
        } else {
            None
        }
    }
}
