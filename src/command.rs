use std::collections::HashMap;

use crate::{error::Error, Backend};

const COMMAND_NEW_NOTE: &str = "new-note";
const COMMAND_DAILY: &str = "daily";
const COMMAND_WEEKLY: &str = "weekly";

const TEMPLATE_NEW_NOTE_FILE_NAME: &str = "new_note.md";
const TEMPLATE_DAILY_FILE_NAME: &str = "daily.md";
const TEMPLATE_WEEKLY_FILE_NAME: &str = "weekly.md";

lazy_static::lazy_static! {
    /// All available commands.
    pub static ref COMMANDS: HashMap<&'static str, Box<dyn Command + Send + Sync + 'static>> = HashMap::from([
        (COMMAND_NEW_NOTE, Box::new(NewNoteFromTemplateCommand(TEMPLATE_NEW_NOTE_FILE_NAME)) as Box<dyn Command + Send + Sync + 'static>),
        (COMMAND_DAILY, Box::new(NewNoteFromTemplateCommand(TEMPLATE_DAILY_FILE_NAME))),
        (COMMAND_WEEKLY, Box::new(NewNoteFromTemplateCommand(TEMPLATE_WEEKLY_FILE_NAME))),
    ]);
}

/// A command is anything which can use the backend to something.
#[tower_lsp::async_trait]
pub trait Command {
    async fn run(&self, backend: &Backend) -> Result<(), Error>;
}

// The rest of the document are possible command types.

/// Create a new note from a given template.
pub struct NewNoteFromTemplateCommand(&'static str);

#[tower_lsp::async_trait]
impl Command for NewNoteFromTemplateCommand {
    async fn run(&self, backend: &Backend) -> Result<(), Error> {
        backend.new_note_from_template(self.0).await
    }
}
