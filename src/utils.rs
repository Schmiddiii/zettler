use std::fs::File;
use std::io::{Read, Seek, Write};
use std::path::PathBuf;

use tower_lsp::lsp_types::{Position, Range};

use crate::error::Error;
use crate::path_url::PathUrl;

pub(crate) fn content_for_uri(uri: &PathUrl) -> Result<String, Error> {
    let mut file = File::open(&uri)?;
    let mut content = String::new();
    file.read_to_string(&mut content)?;
    Ok(content)
}

// Extracts the first non-empty, non-yaml line.
pub(crate) fn extract_firstline_for_file(content: &str) -> String {
    let mut lines = content.lines().skip_while(|l| l.is_empty()).peekable();

    if lines.peek() == Some(&"---") {
        lines
            .skip(1)
            .skip_while(|l| *l != "---")
            .skip(1)
            .skip_while(|l| l.is_empty())
            .take_while(|l| !l.is_empty())
            .collect::<Vec<_>>()
            .join("\n")
    } else {
        lines
            .take_while(|l| !l.is_empty())
            .collect::<Vec<_>>()
            .join("\n")
    }
}

pub(crate) fn frontmatter_for_string<S: AsRef<str>>(content: S) -> Option<serde_yaml::Value> {
    let content = content.as_ref();

    let mut lines = content.lines().peekable();
    if lines.peek() == Some(&"---") {
        let content: String = lines
            .skip(1)
            .take_while(|l| l != &"---")
            .fold(String::new(), |acc, l| acc + l + "\n");
        // TODO: Error handling?
        serde_yaml::from_str(&content[..]).ok()
    } else {
        None
    }
}

/// Find in the strings, with char-offset instead of byte-offset.
///
/// Cannot find multi-line needles.
pub(crate) fn find_in_string<S: AsRef<str>>(content: S, needle: S) -> Vec<Range> {
    let content = content.as_ref();
    let needle = needle.as_ref();

    let needle_len = needle.chars().count();

    content
        .lines()
        .enumerate()
        .flat_map(|(i, l)| {
            let chars = l.chars().collect::<Vec<_>>();
            chars
                .windows(needle_len)
                .enumerate()
                .filter(|(_, w)| w.into_iter().collect::<String>() == needle)
                .map(move |(j, _)| (i, j))
                .collect::<Vec<_>>()
        })
        .map(|(i, j)| {
            Range::new(
                Position::new(i as u32, j as u32),
                Position::new(i as u32, j as u32 + needle_len as u32),
            )
        })
        .collect()
}

pub(crate) fn replace_in_file<S: AsRef<str>>(
    uri: &PathUrl,
    needle: S,
    replacement: S,
) -> Result<(), Error> {
    tracing::debug!(
        "Replacing in file: {}",
        PathBuf::from(uri.clone()).display()
    );
    let mut file = File::options().read(true).write(true).open(&uri)?;
    let mut content = String::new();
    file.read_to_string(&mut content)?;

    if content.contains(needle.as_ref()) {
        content = content.replace(needle.as_ref(), replacement.as_ref());
        file.seek(std::io::SeekFrom::Start(0))?;
        file.set_len(0)?;
        file.write(content.as_bytes())?;
    }

    Ok(())
}
