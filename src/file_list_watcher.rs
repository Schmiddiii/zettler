use std::{
    collections::{BTreeSet, VecDeque},
    fmt::Debug,
    sync::{Arc, Mutex, MutexGuard},
};

use futures::StreamExt;
use inotify::{Inotify, WatchMask};

use crate::{error::Error, path_url::PathUrl, FILE_EXTENSION};

/// A struct that always holds an up-to-date (recursive) listing of file contents.
///
/// Note that the current implementation just always queries the entire folder listing when something changes, as a correct implementation based on file events is non-trivial.
#[derive(Clone, Debug)]
pub struct FileListWatcher {
    inner: Arc<Mutex<FileListWatcherInner>>,
}

#[derive(Debug)]
struct FileListWatcherInner {
    path: PathUrl,

    files: BTreeSet<PathUrl>,
    folders: BTreeSet<PathUrl>,
}

impl FileListWatcher {
    pub fn new(path: PathUrl) -> Self {
        Self {
            inner: Arc::new(Mutex::new(FileListWatcherInner {
                path,
                files: BTreeSet::new(),
                folders: BTreeSet::new(),
            })),
        }
    }

    fn inner(&self) -> MutexGuard<FileListWatcherInner> {
        self.inner.lock().expect("poisoned inner file list watcher")
    }

    pub fn path(&self) -> PathUrl {
        self.inner().path.clone()
    }

    pub fn files(&self) -> BTreeSet<PathUrl> {
        self.inner().files.clone()
    }

    pub fn folders(&self) -> BTreeSet<PathUrl> {
        self.inner().folders.clone()
    }

    /// Run the file list watcher in another thread.
    ///
    /// Returns an error if the initial setup was faulty.
    pub fn run(&self) -> Result<(), Error> {
        let clone = self.clone();
        clone.initial_setup()?;
        tokio::spawn(async move {
            let _ = clone.do_run().await;
        });
        Ok(())
    }

    pub async fn do_run(&self) -> Result<(), Error> {
        tracing::info!("running the FileListWatcher");

        let inotify = Inotify::init()?;
        let mut watches = inotify.watches();

        let mut watched_folders = BTreeSet::new();

        for folder in self.folders() {
            tracing::trace!(message = "adding watch for folder", %folder);
            watches.add(
                &folder,
                WatchMask::CREATE | WatchMask::DELETE | WatchMask::MOVE,
            )?;
            watched_folders.insert(folder);
        }

        // Not sure why that buffer is needed.
        // TODO: Dynamic size based on `inotify::get_absolute_path_buffer_size`.
        let buffer = [0; 1024];
        let mut event_stream = inotify.into_event_stream(buffer)?;

        while let Some(_event) = event_stream.next().await.transpose()? {
            // Just rescan the entire folder.
            // Using the event to update the list is non-trivial (e.g. moving directories with files included).
            self.initial_setup()?;

            // Add all new folders in the folder as a watch.
            for folder in self.folders() {
                if !watched_folders.contains(&folder) {
                    tracing::trace!(message = "adding watch for folder", %folder);
                    watches.add(
                        &folder,
                        WatchMask::CREATE | WatchMask::DELETE | WatchMask::MOVE,
                    )?;
                    watched_folders.insert(folder);
                }
            }
        }

        Ok(())
    }

    fn initial_setup(&self) -> Result<(), Error> {
        tracing::info!("doing initial setup for the FileListWatcher");
        let (files, folders) = self.initial_setup_list_files_folders()?;

        let mut inner = self.inner();
        inner.files = files;
        inner.folders = folders;

        Ok(())
    }

    fn initial_setup_list_files_folders(
        &self,
    ) -> Result<(BTreeSet<PathUrl>, BTreeSet<PathUrl>), Error> {
        let mut queue = VecDeque::from([self.path().clone().into()]);

        let mut files = BTreeSet::new();
        let mut folders = BTreeSet::new();
        folders.insert(self.path().clone());

        // Note: This should never deadlock, we don't follow symlinks.
        while let Some(folder) = queue.pop_front() {
            for entry in std::fs::read_dir(folder)? {
                let entry = entry?;
                let file_type = entry.file_type()?;
                let path = entry.path();
                if file_type.is_file() {
                    if path.extension().is_some_and(|s| s == FILE_EXTENSION) {
                        files.insert(path.try_into()?);
                    }
                } else if file_type.is_dir() {
                    queue.push_back(path.clone());
                    folders.insert(path.try_into()?);
                } else {
                    tracing::debug!(
                        message = "Ignoring non-files and non-directories when setting up file list watcher",
                        path = %path.display()
                    );
                }
            }
        }

        Ok((files, folders))
    }
}

#[cfg(test)]
mod test {
    use std::fs::{create_dir, rename, File};

    use super::*;
    use temp_dir::TempDir;

    impl super::FileListWatcher {
        fn assert_correctness(&self) {
            let (expected_files, expected_folders) = self
                .initial_setup_list_files_folders()
                .expect("Success querying expected files and folders");
            let (inner_files, inner_folders) = (self.files(), self.folders());

            let correct = expected_files == inner_files && expected_folders == inner_folders;

            assert!(
                correct,
                "{:?} == {:?}\n{:?} == {:?}",
                expected_files
                    .into_iter()
                    .map(|t| t.to_string())
                    .collect::<Vec<_>>(),
                inner_files
                    .into_iter()
                    .map(|t| t.to_string())
                    .collect::<Vec<_>>(),
                expected_folders
                    .into_iter()
                    .map(|t| t.to_string())
                    .collect::<Vec<_>>(),
                inner_folders
                    .into_iter()
                    .map(|t| t.to_string())
                    .collect::<Vec<_>>()
            );
        }
    }

    #[tokio::test]
    async fn initial_setup() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = TempDir::new()?;
        let path = tmp.path();
        let a: PathUrl = path.join("a.md").try_into()?;
        let b: PathUrl = path.join("b.md").try_into()?;
        let _ = File::create(&a)?;
        let _ = File::create(&b)?;

        let watcher = FileListWatcher::new(path.to_owned().try_into()?);
        watcher.run()?;

        assert_eq!(watcher.files().into_iter().collect::<Vec<_>>(), vec![a, b]);

        Ok(())
    }

    #[tokio::test]
    async fn create_new() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = TempDir::new()?;
        let path = tmp.path();

        let watcher = FileListWatcher::new(path.to_owned().try_into()?);
        watcher.run()?;

        // Allow it to initialize the inotify watcher.
        tokio::task::yield_now().await;

        let _a = File::create(&path.join("a.md"))?;
        let _b = File::create(&path.join("b.md"))?;

        // Process all tasks.
        tokio::task::yield_now().await;
        tokio::task::yield_now().await;

        watcher.assert_correctness();

        Ok(())
    }

    #[tokio::test]
    async fn create_new_in_dir() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = TempDir::new()?;
        let path = tmp.path();

        let watcher = FileListWatcher::new(path.to_owned().try_into()?);
        watcher.run()?;

        // Allow it to initialize the inotify watcher.
        tokio::task::yield_now().await;

        let dir_path = path.join("dir");
        let _d = create_dir(&dir_path)?;

        tokio::task::yield_now().await;
        tokio::task::yield_now().await;

        let _a = File::create(&dir_path.join("a.md"))?;

        tokio::task::yield_now().await;

        let _b = File::create(&path.join("b.md"))?;

        // Process all tasks.
        tokio::task::yield_now().await;

        watcher.assert_correctness();

        Ok(())
    }

    #[tokio::test]
    async fn move_dir_and_file() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = TempDir::new()?;
        let path = tmp.path();

        let watcher = FileListWatcher::new(path.to_owned().try_into()?);
        watcher.run()?;

        // Allow it to initialize the inotify watcher.
        tokio::task::yield_now().await;

        let dir_path = path.join("dir");
        let dir2_path = path.join("dir2");
        let _d = create_dir(&dir_path)?;

        tokio::task::yield_now().await;
        tokio::task::yield_now().await;

        let _a = File::create(&dir_path.join("a.md"))?;

        tokio::task::yield_now().await;

        let b_path = path.join("b.md");
        let b2_path = path.join("b2.md");
        let _b = File::create(&b_path)?;

        tokio::task::yield_now().await;

        rename(&dir_path, &dir2_path)?;

        tokio::task::yield_now().await;
        tokio::task::yield_now().await;

        rename(&b_path, &b2_path)?;

        tokio::task::yield_now().await;
        tokio::task::yield_now().await;

        watcher.assert_correctness();

        Ok(())
    }
}
