use std::collections::HashMap;

use chrono::{Datelike, Local};
use tower_lsp::lsp_types::Position;

const UUID_KEY: &str = "uuid";
const UUID_FORMAT: &str = "%Y%m%d%H%M";
const DATE_KEY: &str = "date";
const DATE_FORMAT: &str = "%F";
const HDATE_KEY: &str = "hdate";
// hdate format defined in hdate_format
const YEAR_KEY: &str = "year";
const YEAR_FORMAT: &str = "%Y";
const WEEK_KEY: &str = "week";
const WEEK_FORMAT: &str = "%V";

const CURSOR_KEY: &str = "cursor";

const fn hdate_format(day: u32) -> &'static str {
    match day {
        1 | 21 | 31 => "%A, %B %-dst, %Y",
        2 | 22 => "%A, %B %-dnd, %Y",
        3 | 23 => "%A, %B %-drd, %Y",
        _ => "%A, %B %-dth, %Y",
    }
}

pub struct Templating;

impl Templating {
    /// Template a string.
    pub fn template<S: AsRef<str>>(s: S) -> String {
        let mut s = s.as_ref().to_owned();
        for (key, value) in Self::templates() {
            s = s.replace(&Self::format_key(key), &value);
        }
        s
    }

    /// A map from format keys to their replacement values.
    fn templates() -> HashMap<&'static str, String> {
        let now = Local::now();
        HashMap::from([
            (UUID_KEY, now.format(UUID_FORMAT).to_string()),
            (DATE_KEY, now.format(DATE_FORMAT).to_string()),
            (HDATE_KEY, now.format(hdate_format(now.day())).to_string()),
            (YEAR_KEY, now.format(YEAR_FORMAT).to_string()),
            (WEEK_KEY, now.format(WEEK_FORMAT).to_string()),
        ])
    }

    fn format_key(key: &str) -> String {
        format!("{{{{{}}}}}", key)
    }

    /// Find the position where the cursor must be located.
    ///
    /// Furthermore, remove the cursor template from the string.
    pub fn cursor(content: &mut String) -> Option<Position> {
        let cursor_key = Self::format_key(CURSOR_KEY);
        // Note this is more complex than just a single `find` due to UTF-8 indexing in LSPs and byte-indexing in Rust.
        // Note that this is probably also highly inefficient, e.g. collecting the comparison String for each char.
        let cursor = content
            .lines()
            .enumerate()
            .filter(|(_, l)| l.contains(&cursor_key))
            .map(|(i, l)| {
                (
                    i,
                    l.chars()
                        .collect::<Vec<_>>()
                        .windows(CURSOR_KEY.len()) // Note: CURSOR_KEY is ASCII, therefore `len` instead of `.chars().count()`.
                        .enumerate()
                        .filter(|(_, w)| w.iter().collect::<String>() == CURSOR_KEY)
                        .map(|(j, _)| j)
                        .next()
                        .expect("Cursor key to be contained in the line"),
                )
            })
            .next();

        *content = content.replace(&cursor_key, "");

        cursor.map(|(line, column)| Position::new(line as u32, column as u32))
    }
}
